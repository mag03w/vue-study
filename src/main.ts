import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import VueCompositionApi from '@vue/composition-api';
import lodash from 'lodash';
import VModal from 'vue-js-modal';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import Konva from 'vue-konva';

Vue.use(Konva);
Vue.config.productionTip = false;
Vue.use(VueCompositionApi);
Vue.use(lodash);
Vue.use(VModal);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
