export type Request = {
  id: number;
  sendType: 'GET' | 'POST' | 'PUT' | 'DELETE';
  uri: string;
  requestDate: string;
  requestBody: {
    key: string;
    value: string;
  }[];
};
