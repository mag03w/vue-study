import axios, { AxiosResponse } from 'axios';

export default (resource: string): { [key: string]: () => Promise<AxiosResponse<string[]>> } => {
  return {
    get: (): Promise<AxiosResponse<string[]>> => {
      return axios.get(`${process.env.VUE_APP_URI}/api/search/${resource}`);
    },
  };
};
